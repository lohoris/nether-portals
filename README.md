# Nether Portals #

This is a nether portal calculator, used to determine which portal you will get to when taking one.

## Disclaimer ##

**This is extremely raw.**

It redefines the concept of "unpolished" itself.

**This is not even a program meant to be run** or compiled: you can only use it via Unity Editor.

## Usage ##

Download. Open in Unity Editor.

Place portals. Do stuff.

Look at the scene for an example. Improve this if you wish to.