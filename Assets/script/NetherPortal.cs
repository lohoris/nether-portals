﻿/*
	© 2015 Goblinsama S.r.l.s., All rights reserved
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class NetherPortal : MonoBehaviour
{
	private static Color gizmoColorOverworld = new Color(0,1,0,1);
	private static Color gizmoColorNether = new Color(1,0,0,1);
	private const float scale = 4f;
	
	public NetherPortal nearest;
	
	[SerializeField] private bool isNether;
	[SerializeField] private bool isPlanned = false;
	[SerializeField] private bool doCheck = false;
	
	private Vector3 baseSize = new Vector3(4,5,1);
	
	private Color gizmoColor {
		get {
			Color col = isNether ? gizmoColorNether : gizmoColorOverworld;
			if (isPlanned)
				col.b = 1;
			return col;
		}
	}
	private Vector3 size {
		get {
			return baseSize * scale;
		}
	}
	public Vector3 noYPosition {
		get {
			Vector3 pos = transform.position;
			pos.y=0;
			return pos;
		}
	}
	private Vector3 combinedPosition {
		get {
			return isNether ? noYPosition*8 : noYPosition;
		}
	}
	private Vector3 oppositePosition {
		get {
			return isNether ? noYPosition*8 : noYPosition/8f;
		}
	}
	
	void Awake ()
	{
		doCheck = false;
	}
	void Update ()
	{
		if (doCheck)
		{
			Check(true);
			doCheck = false;
		}
	}
	void OnDrawGizmos ()
	{
		Gizmos.color = gizmoColor;
		Gizmos.DrawWireCube( combinedPosition, size*scale );
	}

	public bool Check (bool logAnyway=false)
	{
		Plane checkPlane = isNether ? (Plane)FindObjectOfType<Overworld>() : (Plane)FindObjectOfType<Nether>();
		
		NetherPortal found = checkPlane.FindNearest(oppositePosition);
		if (!found)
		{
		     Debug.LogWarning(this+" found nothing.");
		}
		
		if (found != nearest)
		{
			Debug.Log(this+" new nearest is "+found+" (former: "+nearest+").");
			nearest = found;
			return true;
		}
		else
		{
			if (logAnyway)
			{
				Debug.Log(this+" nearest didn't change ("+nearest+").");
			}
			return false;
		}
	}
}
