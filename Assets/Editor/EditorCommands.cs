﻿/*
	© 2015 Goblinsama S.r.l.s., All rights reserved
*/

using UnityEngine;
using System.Collections;

public class EditorCommands : MonoBehaviour
{
	#if UNITY_EDITOR
	[UnityEditor.MenuItem("Extra/Check all")]
	public static void CheckAll ()
	{
		bool any = false;
		foreach (NetherPortal por in MonoBehaviour.FindObjectsOfType<NetherPortal>())
		{
			any = por.Check() || any;
		}
		if (!any)
		{
			Debug.Log("No change.");
		}
	}
	#endif
}
