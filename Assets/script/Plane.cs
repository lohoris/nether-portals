﻿/*
	© 2015 Goblinsama S.r.l.s., All rights reserved
*/

using UnityEngine;
using System.Collections;

public abstract class Plane : MonoBehaviour
{
	private NetherPortal[] _portals;
	private NetherPortal[] portals {
		get {
			if (_portals.Length<1) {
				_portals = GetComponentsInChildren<NetherPortal>(true);
				if (_portals.Length<1)
				{
					throw new UnityException(this+" couldn't find any portal.");
				}
			}
			return _portals;
		}
	}
	
	public NetherPortal FindNearest (Vector3 position)
	{
		NetherPortal ret = null;
		float distance = Mathf.Infinity;
		foreach (NetherPortal por in portals)
		{
			float dist = Vector3.Distance(position,por.noYPosition);
			if (dist < distance)
			{
				ret = por;
				distance = dist;
			}
		}
		return ret;
	}
}
